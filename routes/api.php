<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Cliente;
use App\Endereco;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/clientes', function () {
    $clientes = Cliente::all();
    foreach($clientes as $c) {
        dd('nome: ' . $c->nome . ' - número: ' .$c->endereco->numero);
    }
});

Route::get('/enderecos', function () {
    $ends = Endereco::all();
    foreach($ends as $e) {
        echo "<p> ID cliente: " . $e->cliente_id . "</p>";
        echo "<p>Nome: " . $e->cliente->nome . "</p>";
        echo "<p>Telefone: " . $e->cliente->telefone. "</p>";
        echo "<p>Rua: " . $e->rua . "</p>";
        echo "<p>Número: " . $e->numero . "</p>"; 
        echo "<p>Bairro: " . $e->bairro . "</p>";
        echo "<p>Cidade: " . $e->cidade . "</p>";
        echo "<p>UF: " . $e->uf . "</p>";
        echo "<p>Cep: " . $e->cep . "</p>";
        echo "<hr>";
    }
});

Route::get('/clientes/json', function () {
    $clientes = Cliente::with(['endereco'])->get();
    return $clientes->toJson();
});