<?php

use Illuminate\Database\Seeder;

class EnderecosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Depreciado
        //Utilizando metodo de popular seed com relacionamento no ClientesTableSeeder
        $users = DB::table('clientes')->pluck('id');
        $leagth_users = count($users);;
        //dd($users[random_int(0,$leagth_users - 1)]);
        DB::table('enderecos')->insert([
            'cliente_id' => $users[random_int(0,$leagth_users - 1)],
            'rua' => Str::random(4),
            'numero' => random_int(1000,9999),
            'bairro' => Str::random(5),
            'cidade' => Str::random(5),
            'uf' => Str::random(2),
            'cep' => random_int(64000000,64999999),
            'created_at' => Carbon\Carbon::now()
            //'password' => bcrypt('password'),
        ]);
    }
}
