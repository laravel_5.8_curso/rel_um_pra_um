<?php

use Illuminate\Database\Seeder;
use App\Cliente;
use App\Endereco;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $c = new Cliente();
        $c->nome = Str::random(10);
        $c->telefone = random_int(900000000,999999999);
        $c->created_at = Carbon\Carbon::now();
        $c->save();

        $e = new Endereco();
        $e->rua = Str::random(4);
        $e->numero = random_int(1333,1999);
        $e->bairro = Str::random(5);
        $e->cidade = Str::random(5);
        $e->uf = Str::random(2);
        $e->cep = random_int(64000000, 64999999);
        $e->created_at = Carbon\Carbon::now();
        $c->endereco()->save($e);

    }
}
